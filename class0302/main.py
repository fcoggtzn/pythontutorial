from abc import ABC, abstractmethod


class InitSensor:
    def init(self):
        print("The sensor has been initialized ")


class Sensor(ABC):
    x = 5

    def __init__(self):
        Sensor.x = Sensor.x + 1
        self.x = Sensor.x

    @abstractmethod
    def getValue(self):
        pass


class DoorSensor(Sensor, InitSensor):
    def getValue(self):
        return self.x


class TemperatureSensor(Sensor):
    def getValue(self):
        return str(self.x) + "ºC"


class HumiditySensor(Sensor, InitSensor):
    def getValue(self):
        return str(self.x) + " ppm"


x = DoorSensor(), TemperatureSensor(), HumiditySensor()
y = x[0], x[1], x[2]

for i in x:
    if hasattr(i, "init"):
        print(i.init())
    print(i.getValue())

if x == y:
    print("x es igual a y")
