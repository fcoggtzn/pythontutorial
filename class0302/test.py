from calculator_object import Calculator

calculator = Calculator()

calculator.add(5)
calculator.add(7.6)
calculator.subtract(7.6)

calculator.add("x")
print(calculator.memory)

import datetime

x = datetime.datetime.now()
print(x)


import json

# some JSON:
x =  '{ "name":"John", "age":30, "city":"New York"}'

# parse x:
y = json.loads(x)

# the result is a Python dictionary:
print(y)