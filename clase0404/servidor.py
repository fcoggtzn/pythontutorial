from flask import Flask, request, json
from clase0404.controller import Sensor
from clase0404.modelo import systema

app = Flask(__name__)


@app.route("/")
def read_sensor():
    semsors = systema
    x = json.dumps(semsors)
    response = app.response_class(
        response=x,
        status=200,
        mimetype='application/json'
    )
    return response


if __name__ == "__main__":
    app.run(port=2020)

