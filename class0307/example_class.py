class C:
    def __init__(self):
        self._x = None

    @property
    def x(self):
        """I'm the 'x' property."""
        return self._x

    @x.setter
    def x(self, value):
        self._x = value + 1
        print("entro en set")

    @x.deleter
    def x(self):
        del self._x

    def __str__(self):
        return str(self._x)

    def __eq__(self, other):
        return True


claseC = C()

print(claseC.x)
claseC.x = 1

print(str(claseC))

print( claseC ==  "hola")

x= list(range(0,100,2))
print(x)


x="Hugo Paco Luis"

print(x[0:4])
print(x[4:8])
