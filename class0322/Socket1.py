import socket
import random

def main():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('0.0.0.0', 1729))
    server_socket.listen(1)
    (client_socket, server_socket) = server_socket.accept()

    done = False
    while not done:
        port = client_socket.recv(4096)
        client_socket.send('i got the port' + port)
        port = int(port)

        if port != 1:
            server_socket.bind(('0.0.0.0', port))
            continue
        else:
            done = True


if __name__ == '__main__':
    main()