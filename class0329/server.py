from flask import Flask, request

MESSAGE = "ssssh, this is secret"
led_status = "off"
app = Flask(__name__)


@app.route("/")
def get_secret_message():
    return MESSAGE + "\n"


@app.route("/led", methods=['GET'])
def get_led_status():
    return led_status + "\n"


@app.route("/led", methods=['POST'])
def post_led_status():
    global led_status
    print(request.form['value_led'])
    led_status = request.form['value_led']

    return '{commandStatus:"ok"}'


if __name__ == "__main__":
    app.run(port=5684)
